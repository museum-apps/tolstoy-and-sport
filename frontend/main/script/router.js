import Vue from 'vue'
import VueRouter from 'vue-router'

import main from './route/main'

export default router()

function router () {
	Vue.use(VueRouter)

	return new VueRouter({
		base: '/',
		mode: 'hash',
		routes: [
			main
		]
	})
}