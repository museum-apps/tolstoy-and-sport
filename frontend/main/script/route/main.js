import Main from ':main/view/Main'

export default {
  path: '/',
  name: 'Main',
  component: Main
}