import Vue from 'vue'

import App from ':main/App.vue'
import router from ':main/script/router'
import store from ':main/script/store'

new Vue({
  router,
  store,
  render: handle => handle(App)
}).$mount('#body')
