const pathAlias = require('path-alias')
const { app } = require('electron')

const events = pathAlias('@app/core/events')
const window = pathAlias('@app/core/window')

app.on('ready', () => {
  events.install()
  
  
  const main = window.main.create()
})