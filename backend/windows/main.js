'use as window'

const pathAlias = require('path-alias')
const { BrowserWindow } = require('electron')

const local = {}
const config = pathAlias('@app/config')

module.exports = { install }

function install (list) {
  list.main = { create }
}

function create () {
  const window = createWindow()
  
  window.on('ready-to-show', show)
  window.loadURL(config.starts.main)

  local.window = window
  return window
}

function createWindow () {
  const { height, width } = config.display.size()

  return new BrowserWindow({
    height, width,
    show: false
  })
}

function show () {
  const window = local.window

  window.setMenu(null)
  window.show()
  window.maximize()
}