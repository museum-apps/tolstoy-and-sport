const { screen } = require('electron')

module.exports = {
  display: { size },
  starts: {
    main: 'http://127.0.0.1:8090/'
  }
}

function size () {
  const display = screen.getPrimaryDisplay()

  return display.size
}