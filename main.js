const path = require('path')
const pathAlias = require('path-alias')

const rootPath = __dirname
const appPath = path.join(rootPath, 'backend')

pathAlias.setAlias('root', rootPath)
pathAlias.setAlias('app', appPath)

pathAlias('@app/main')