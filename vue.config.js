const path = require('path')
const root = __dirname

module.exports = {
    publicPath: '/',
    filenameHashing: false,
    productionSourceMap: false,
    outputDir: path.join(root, 'interface'),

    pages: getPages(),
    configureWebpack: getConfigureWebpack()
}

function getPages () {
    return {
        index: {
            entry: path.join(root, 'frontend', 'main.js'),
            template: path.join(root, 'public', 'index.pug'),
            filename: 'index.html'
        }
    }
}

function getConfigureWebpack () {
    return {
        resolve: {
            alias: {
                'root': root,
                ':root': root,

                'src': path.join(root, 'frontend'),
                ':src': path.join(root, 'frontend'),

                'assets': path.join(root, 'frontend/assets'),
                ':assets': path.join(root, 'frontend/assets'),

                'main': path.join(root, 'frontend/main'),
                ':main': path.join(root, 'frontend/main')
            }
        }
    }
}